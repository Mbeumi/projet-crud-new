<?php session_start(); ?>




<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	<style type="text/css">
		ul li{
		list-style-type: none !important;
		margin: 20px;
		}
		.image{
			height: 100px;
			width: 100px;
		}
	</style>
</head>
<body class="corps ">

	<?php 
		if(isset($_SESSION['USER'])){ ?>
			<div class="container">
				<div class="row">
					<nav class="navbar navbar-expand-lg ">
				  		<div class=" navbar-collapse pull pull-right" >
					      	<ul class="navbar-nav">
						      	<li><span class="nom" style="text-align: justify; font-weight: bold;position: relative; top:35px;"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
						      	<li class=" dropdown ">
						       		<a  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >
						          		<?php echo "<img class='profil img-circle image' src='../images/".$_SESSION['USER']['photo']."'>" ?>
						        	</a>
							        <ul class="dropdown-menu">
		            					<li><a href="profil.php">Mon profil</a></li>
		            					<li><a href="deconnexion.php">deconnexion</a></li>
		          					</ul>
						      	</li>
						    </ul>
					      	
					    
					 	 </div>
					</nav>
				</div>
			</div>
		<?php }else{
				header('location:connexion.php');
			} ?>
	

	


		
	 
<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>