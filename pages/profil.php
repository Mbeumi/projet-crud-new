 <?php 
 	include('header_account.php');
  ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<title>profil</title>
</head>
<style type="text/css">
        .img_visualise{
            width: 100px; 
            height: 100px; 
            border-radius: 50%;
        }
        .load_img{
            position: relative; top: -25px; left: 85px;
            /z-index: 2;/
            border: 1px solid;
            width: 25px;
            height: 25px;
            border-radius: 50%;
            /background-color: blanchedalmond;/
            background-color: #f8f8f8;
        }
        .fa-refresh{
            position: relative; left: 5px; top: 2px;
        }
        .libele{
            /border: 1px solid;/
            display: inline-block;
            width: 70px;
        }
        #new_image{
            /opacity: 0;/
            height: 0px;
        }
        label[class]{
            /background-color: red;/
        }
</style>
<body>
<div class="countainer">
	<div class="col-md-8 col-md-push-1 col-sm-8 c0l-sm-push-1" style="border: 1px solid;">
		<h3>Informations Personnelles:</h3>

		<form action="traitement_modif.php" method="POST" enctype="multipart/form-data">
		<!-- <form action="" method="POST" enctype="multipart/form-data"> -->
			 <!-- <label for="new_image" class="load_img"  title="Editer"><span class="fa fa-refresh"></span></label> -->
		    <label for="new_image"><img id="image_depart" class="img_visualise" <?php echo "src='../images/".$_SESSION['USER']['photo']."'" ?>></label>
			<input id="new_image" type="file" name="photo" accept="image/png, image/jpeg, image/jpg" value='<?php echo $_SESSION['USER']['photo']?>'>
			
			<label> 
				<label for="nom" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label> 
				<span class="libele">Nom :</span> 
				<input id="nom" style="background-color: inherit; border: none;" type="text" name="nom" value=<?php echo "'".$_SESSION['USER']['nom']."'" ?>>
			</label> <br>

			<label>
				<label for="prenom" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label>
				<span class="libele">Prenom :</span>
				<input id="prenom" style="background-color: inherit; border: none;"  type="text" name="prenom" value=<?php echo "'".$_SESSION['USER']['prenom']."'" ?>>
			</label> <br>

			<label>
				<label for="email" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label>
				<span class="libele">Email :</span>
				<input id="email" style="background-color: inherit; border: none;"  type="email" name="email" value=<?php echo "'".$_SESSION['USER']['email']."'" ?>>
			</label> <br>

			<label>
				<label for="pwd" class="btn btn-xs btn-default pull-right indicate"><span class="fa fa-pencil"></span></label>
				<span class="libele">Pwd :</span>
				<input id="pwd" style="background-color: inherit; border: none;" type="password" name="pwd" placeholder="Mot de passe" value="">
			</label> <br>

			<input type="reset" value="Annuler" class="btn btn-default btn-sm">	<input type="submit" value="Confirmer" class="btn btn-default btn-sm">
		
		</form>
		
		<div class="editer btn btn-default" id="edit">Modifier Mes informations</div>
	</div>

	<script type="text/javascript">
		var bouton = document.getElementById('edit'),
			input = document.getElementsByTagName('input'),
			label = document.getElementsByClassName('indicate'),
			imgProfil = document.getElementById('image_depart');

			// console.log(imgProfil);
		var Zimage = input[0], Znom = input[1], Zprenom = input[2], Zmail = input[3], Zpwd = input[4], reset = input[5], submit = input[6];

		//Situation de depart
		function cache(){  // Mode consultation des infos
			Zimage.setAttribute('disabled','true');
			Zimage.previousElementSibling.removeAttribute('title');
			Znom.setAttribute('disabled','true');
			Zprenom.setAttribute('disabled','true');
			Zmail.setAttribute('disabled','true');
			Zpwd.setAttribute('disabled','true');
			reset.style.display='none';
			submit.style.display='none';
			bouton.style.display='inline-block';
			Zimage.previousElementSibling.removeChild(Zimage.previousElementSibling.firstChild);
			Zimage.previousElementSibling.appendChild(imgProfil);
			for (var i = label.length - 1; i >= 0; i--) {
				label[i].style.display='none';
			// label[i].style.color='red';
			}
		}

		function affiche (){  // Mode Edition des infos
			Zimage.removeAttribute('disabled');
			Zimage.previousElementSibling.setAttribute('title','Editer');
			Znom.removeAttribute('disabled');
			Zprenom.removeAttribute('disabled');
			Zmail.removeAttribute('disabled');
			Zpwd.removeAttribute('disabled');
			reset.style.display='inline-block';
			submit.style.display='inline-block';
			bouton.style.display='none';
			for (var i = label.length - 1; i >= 0; i--) {
				label[i].style.display='block';
				label[i].setAttribute('title','Editer');
			// label[i].style.color='red';
			}
		}

		function view_newimage(){
			// Supprimons le contenu de la zone d'affichage
			while(Zimage.previousElementSibling.firstChild) {
				Zimage.previousElementSibling.removeChild(Zimage.previousElementSibling.firstChild);
			}
			//
			var image = document.createElement('img'); //on cree in noeud, un element, une balise image
			image.setAttribute('class','img_visualise'); // on attribue a notre balise une classe
			image.src = URL.createObjectURL(Zimage.files[0]); // on recupere le chemin indicant la source de notre image
			Zimage.previousElementSibling.appendChild(image); // on definit notre image comme element enfant de sa zone d'affichage soit on positionne notre noeud dans le dom
		}

		function editable(){

			affiche();
			Zimage.addEventListener('change', view_newimage);
		}
		//on se met en mode consultation
		cache();

		bouton.addEventListener('click', editable); // on active le mode edition.
		reset.addEventListener('click', cache); // on reviens en mode consultation.
		// submit.addEventListener('click', cache); // on reviens en mode consultation.
		// console.log(label);
		// console.log(Znom);
		// Znom.removeAttribute('disabled');
		// console.log(Znom);	
	</script>
</body>
</html>