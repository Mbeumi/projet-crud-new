
<?php 
	session_start();
 ?>
<?php
    // verification de reception des donnees et appropriation de celles ci
    if(isset($_POST)){
        if (isset($_POST['email']) && isset($_POST['pwd'])) {
            $mail = htmlspecialchars($_POST['email']);
            $pwd =($_POST['pwd']) ;

        // Verification d'existence de l'adresse mail
            // Connection a la BDD
            $bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));

            // Recuperer le champ ayant la valeur de mail correspondant
            $dataRecup=$bdd->prepare('SELECT * FROM utilisateur WHERE email = ?');
            $dataRecup->execute(array($mail));

            if ($response=$dataRecup->fetch()) {    //si email existe dans la base
                $_SESSION = array();
                $_SESSION['USER']=$response;
                // echo('utilisateur existant');

                // verification de correspondance du pwd 
                if ($_SESSION['USER']['pwd']==$pwd) {
                    // redirection vers la page utilisateur.
                    if($_SESSION['USER']['niveau']==1){
                        header('location:header_account.php');
                    }else{
                        $_SESSION['message_error']="Utilisateur inexistant!";
                        header('location:connexion.php');

                    }
                }else{
                    $_SESSION['message_error']="Mot de passe incorrect";
                    header('location:connexion.php');
                }
            }else{
                $_SESSION = array();
                $_SESSION['message_error']="Compte inexistant";
                header('location:connexion.php');
                // echo('utilisateur non existant');
            }
           }
    }