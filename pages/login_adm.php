<?php 
	session_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>connexion</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
</head>
<body class="body" style="background:url('../images/images.jpg'); background-size: cover; background-repeat: no-repeat;">



	<div class="container-fluid ">
		
		<div class="row ">
			<h2></h2>
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10" style="height: 400px !important; border-radius: 30px;">
				<h3>CONNEXION</h3>

				<center> 

				<?php  
					if(isset($_SESSION['message_error'])){
					echo "<div style='color:red' >".$_SESSION['message_error']."</div>";
						session_destroy();  	
					}
				
				?>


				</center>
				<form enctype="multipart/form-data" method="post" action="traitement_admin.php" id="myform" >
					<div class="row">
						<div class="col-md-offset-1 col-md-10 col-xm-offset-1 col-xm-10 col-xs-offset-1 col-xs-10 form2">
							<span class="glyphicon glyphicon-envelope"></span>
							<label>email</label>
							<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
							<p></p>
							<span class="glyphicon glyphicon-lock"></span>
							<label>mot de passe</label>
							<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required=""><br>
							<p></p><br>
							<input  type="submit"  class="  btn btn-block btn-info btn-success">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	



</body>
</html>