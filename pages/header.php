<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Header</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style1.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
</head>
<body class="body" style="background:url('images/3175.jpg'); background-size: cover; background-repeat: no-repeat;">
	<div class="container-fluid">
		<div class="row nav_bar">
			<div class="col-md-12 col-xs-10">
				<nav class="navbar navbar-default pull pull-right nav_bb">
						<button type="button" class=" navbar-toggle text-center" id="but1" data-toggle="collapse" data-target=".navbar-collapse" >
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<ul class="nav navbar-nav collapse navbar-collapse"style="text-align: center; z-index: 4;">
							<li class=""><a href="pages/inscription.php" class="btn btn-info btn_a" style="color: white">INSCRIPTION</a></li>
							<li class=""><a href="pages/connexion.php"  class=" btn btn-info btn_a" style="color: white">CONNEXION</a></li>
						</ul>
				</nav>
			</div>
		</div>
		<h1 style="font-weight: bold; text-align: center; margin-top: 150px; font-size: 100px;"> WELCOME</h1>
	</div>
</body>
</html>